﻿using DBMC.Components;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace DBMC.Components
{
    /// <summary>
    /// Messages.xaml 的交互逻辑
    /// </summary>
    public partial class Messages : UserControl
    {
        public Messages()
        {
            InitializeComponent();
        }

        public void Show(string message)
        {
            MessageText.Text = message;
            Storyboard storyboard = (Storyboard)Resources["SlideDownStoryboard"];
            storyboard.Begin();
        }
    }
}
public class NotificationManager
{
    private static NotificationManager _instance;
    private Messages _notificationControl;

    public static NotificationManager Instance => _instance ?? (_instance = new NotificationManager());

    private NotificationManager()
    {
    }

    public void Initialize(Messages notificationControl)
    {
        _notificationControl = notificationControl;
    }

    public void ShowNotification(string message)
    {
        Application.Current.Dispatcher.Invoke(() =>
        {
            _notificationControl.Show(message);
        });
    }
}