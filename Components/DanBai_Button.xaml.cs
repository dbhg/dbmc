﻿using System.Windows;
using System.Windows.Controls;

namespace DBMC.Components
{
    /// <summary>
    /// DanBai_Button.xaml 的交互逻辑
    /// </summary>
    public partial class DanBai_Button : UserControl
    {
        // 定义一个公共事件，外部可以订阅这个事件
        public event RoutedEventHandler ButtonClick;

        public DanBai_Button()
        {
            InitializeComponent();
        }

        // 这个方法用于触发ButtonClick事件
        protected virtual void OnButtonClick()
        {
            ButtonClick?.Invoke(this, new RoutedEventArgs());
        }

        private void InnerButton_Click(object sender, RoutedEventArgs e)
        {
            OnButtonClick();
        }
    }
}
