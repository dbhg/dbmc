﻿using AduSkin.Controls.Metro;
using DBMC_Core;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DBMC
{
    /// <summary>
    /// AddLocalUser.xaml 的交互逻辑
    /// </summary>
    public partial class AddLocalUser : Window
    {
        private bool isDragging;
        private Point startPoint;
        public string PlayerName, UUID;

        public AddLocalUser()
        {
            InitializeComponent();
            Loaded += delegate { LauncherCore.WriteLog($"启动器核心：添加离线账号 - 窗口打开正常"); };
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isDragging = true;
            startPoint = e.GetPosition(this);
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Point currentPoint = e.GetPosition(this);
                this.Left += currentPoint.X - startPoint.X;
                this.Top += currentPoint.Y - startPoint.Y;
            }
        }

        private void Window_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            isDragging = false;
        }

        private void ExitApp_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Minimize_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void Check_Click(object sender, RoutedEventArgs e)
        {
            if (UserName.Text == "")
            {
                LauncherCore.WriteLog($"启动器核心：添加离线账号 - 提交失败，因为用户名为空");
                AduMessageBox.Show("用户名不能为空", null, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            DialogResult = true;
            Close();
        }

        private async void Name_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            await Task.Delay(1000);
            if (UserName.Text == "")
            {
                Check.IsEnabled = false;
                return;
            }
            else
            {
                if (uuid.Text == "")
                {
                    Guid newGuid = Guid.NewGuid();
                    string uuidString = newGuid.ToString("N");
                    uuid.Text = uuidString;
                    UUID = uuid.Text;
                    LauncherCore.WriteLog($"启动器核心：添加离线账号 - UUID为空已自动生成：{UUID}");
                }
                else
                {
                    UUID = uuid.Text;
                    LauncherCore.WriteLog($"启动器核心：添加离线账号 - UUID非空为：{UUID}");
                }
                PlayerName = UserName.Text;
                Check.IsEnabled = true;
            }
        }
    }
}
