﻿using AduSkin.Controls.Metro;
using DBMC_Core;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace DBMC
{
    /// <summary>
    /// StartToTools.xaml 的交互逻辑
    /// </summary>
    public partial class StartToTools : Window
    {
        public ObservableCollection<ImageItem> ImageItems { get; set; } = new ObservableCollection<ImageItem>();

        private bool isDragging;
        private Point startPoint;

        [DllImport("User32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, UIntPtr dwExtraInfo);

        private const int SW_RESTORE = 9;

        private enum KeyboardKeyStates
        {
            VK_CONTROL = 0x11,
            VK_V = 0x56,
            KEYEVENTF_KEYUP = 0x0002
        }

        public StartToTools()
        {
            InitializeComponent();
            Loaded += Window_Loaded;
            DataContext = this;
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isDragging = true;
            startPoint = e.GetPosition(this);
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Point currentPoint = e.GetPosition(this);
                this.Left += currentPoint.X - startPoint.X;
                this.Top += currentPoint.Y - startPoint.Y;
            }
        }

        private void Window_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            isDragging = false;
        }

        private void ExitApp_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Minimize_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
           LauncherCore.WriteLog($"启动器核心：造图小工具 - 窗口打开正常");
            this.Left = 10; // 窗体左边缘与屏幕左边缘对齐
            this.Top = 10 + SystemParameters.FullPrimaryScreenHeight - this.Height; // 窗体顶部位置
            try
            {
                LauncherCore.WriteLog($"启动器核心：造图小工具 | 表情包大厅 - 加载表情包中...");
                List<ImageItem> imageItems = await GetImageItemsAsync();
                await LoadImagesAsync(imageItems);
            }
            catch (Exception ex)
            {
                // Handle exceptions (e.g., log them, show message to user, etc.)
                Console.WriteLine($"An error occurred: { ex.Message}");
            }
        }

        private async Task<List<ImageItem>> GetImageItemsAsync()
        {
            RestClient client = new RestClient("https://api.dbhg.top/api/emojipack/getAll");
            RestRequest request = new RestRequest
            {
                Method = Method.Post,
                Timeout = 30000
            };
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            // 构建请求体对象
            var requestBody = new
            {
                apikey = "10a2a789976e64a1c9455e2d9"
            };
            request.AddJsonBody(JsonConvert.SerializeObject(requestBody));
            RestResponse response = await client.ExecuteAsync(request);
            if (!response.IsSuccessful)
            {
                EmojipackInfo.Visibility = Visibility.Visible;
                EmojipackLoading.Visibility = Visibility.Hidden;
                EmojipackInfoIcon.Visibility = Visibility.Visible;
                EmojipackTips.Text = "未连接到服务器";
                throw new Exception($"API request failed: {response.ErrorMessage}");
            }
            dynamic responseContent = JsonConvert.DeserializeObject(response.Content);
            dynamic imagesJson = Convert.ToString(responseContent.data);
            return JsonConvert.DeserializeObject<List<ImageItem>>(imagesJson);
        }

        public async Task LoadImagesAsync(IEnumerable<ImageItem> imageItems)
        {
            int total = imageItems.Count();
            int successCount = 0;
            int failureCount = 0;

            await Task.Run(async () =>
            {
                foreach (var imageItem in imageItems)
                {
                    try
                    {
                        var image = await LoadImageAsync(imageItem.Url);
                        await Dispatcher.InvokeAsync(() =>
                        {
                            imageItem.Image = image; // Assuming you've added this property to your ImageItem class
                            ImageItems.Add(imageItem);
                            successCount++;
                        });
                    }
                    catch
                    {
                        await Dispatcher.InvokeAsync(() =>
                        {
                            failureCount++;
                        });
                    }
                    await Dispatcher.InvokeAsync(async () =>
                    {
                        if (successCount >= 5 && failureCount >= 0)
                        {
                            EmojipackInfo.Visibility = Visibility.Visible;
                            EmojipackLoading.Visibility = Visibility.Visible;
                            EmojipackInfoIcon.Visibility = Visibility.Hidden;
                            EmojipackTips.Text = $"进度：{successCount}/{failureCount}/{total}";
                            await Task.Delay(2000);
                            EmojipackInfo.HorizontalAlignment = HorizontalAlignment.Right;
                            EmojipackInfo.VerticalAlignment = VerticalAlignment.Bottom;
                            EmojipackInfo.Background = null;
                        } else if (successCount >= 20 && failureCount >= 0)
                        {
                            EmojipackInfo.Visibility = Visibility.Visible;
                            EmojipackLoading.Visibility = Visibility.Visible;
                            EmojipackInfoIcon.Visibility = Visibility.Hidden;
                            EmojipackTips.Text = $"进度：{successCount}/{failureCount}/{total}";
                        }
                        else if (successCount <= 0 && failureCount > 0)
                        {
                            EmojipackInfo.Visibility = Visibility.Visible;
                            EmojipackLoading.Visibility = Visibility.Visible;
                            EmojipackInfoIcon.Visibility = Visibility.Hidden;
                            EmojipackTips.Text = $"进度：{successCount}/{failureCount}/{total}";
                        }
                    });
                }
            });

            await Dispatcher.InvokeAsync(async () =>
            {
                if (total <= 0)
                {
                    EmojipackInfo.Visibility = Visibility.Visible;
                    EmojipackLoading.Visibility = Visibility.Hidden;
                    EmojipackInfoIcon.Visibility = Visibility.Visible;
                    EmojipackTips.Text = "暂时还没有表情包";
                }
                else if (failureCount > successCount)
                {
                    EmojipackInfo.Visibility = Visibility.Hidden;
                    LauncherCore.WriteLog($"启动器核心：造图小工具 | 表情包大厅 - 表情包部分加载完毕：{successCount}/{failureCount}/{total}");
                    NoticeManager.NotificationShow.AddNotification(new NotificationModel()
                    {
                        Title = "造图小助手",
                        Content = $"类型：表情包大厅\n消息：表情包部分加载完毕，有部分未加载成功",
                        NotificationType = AduSkin.Controls.EnumPromptType.Success
                    });
                }
                else if (successCount <= total)
                {
                    EmojipackInfo.Visibility = Visibility.Hidden;
                    LauncherCore.WriteLog($"启动器核心：造图小工具 | 表情包大厅 - 表情包全部加载完毕");
                    NoticeManager.NotificationShow.AddNotification(new NotificationModel()
                    {
                        Title = "造图小助手",
                        Content = $"类型：表情包大厅\n消息：表情包全部加载完毕",
                        NotificationType = AduSkin.Controls.EnumPromptType.Success
                    });
                }
                else if (failureCount <= total)
                {
                    LauncherCore.WriteLog($"启动器核心：造图小工具 | 表情包大厅 - 表情包全部加载失败");
                    NoticeManager.NotificationShow.AddNotification(new NotificationModel()
                    {
                        Title = "造图小助手",
                        Content = $"类型：表情包大厅\n消息：表情包全部加载失败",
                        NotificationType = AduSkin.Controls.EnumPromptType.Success
                    });
                    await Task.Delay(2000);
                    Emojipack.Visibility = Visibility.Collapsed;
                }
            });
        }

        private async Task<BitmapImage> LoadImageAsync(string imageUrl)
        {
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(imageUrl);
                response.EnsureSuccessStatusCode();

                using (var stream = await response.Content.ReadAsStreamAsync())
                {
                    var bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.CacheOption = BitmapCacheOption.OnLoad;
                    bitmap.DecodePixelWidth = 200;
                    bitmap.DecodePixelHeight = 200;
                    bitmap.StreamSource = stream;
                    bitmap.EndInit();
                    bitmap.Freeze(); // 使图片可以在WPF的任何线程中被使用
                    return bitmap;
                }
            }
        }

        private async void ImageSource_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button button && button.DataContext is ImageItem item)
            {
                Clipboard.SetDataObject(item.Url);
                LauncherCore.WriteLog($"启动器核心：造图小工具 | 表情包大厅 - 用户点击ID为{item.Id}的图片，已经复制图片链接，正在准备粘贴到游戏中");
                //Console.WriteLine($"{item.Url} 已复制");
                await SendPasteCommandToProcess(GameStarting.Pid, item.Id);
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;
            //MessageBox.Show($"你选择了 {menuItem.Header}");
        }

        private async Task SendMinecraftCommandToProcessAsync(int processId, string command)
        {
            try
            {
                Process proc = Process.GetProcessById(processId);
                if (proc == null)
                {
                    LauncherCore.WriteLog($"启动器核心：造图小工具 | 快捷指令 - 执行 {command} 指令到游戏中失败，因为未找到游戏进程");
                    //Console.WriteLine("未找到进程");
                    return;
                }

                IntPtr hWnd = proc.MainWindowHandle;
                if (hWnd == IntPtr.Zero)
                {
                    LauncherCore.WriteLog($"启动器核心：造图小工具 | 快捷指令 - 执行 {command} 指令到游戏中失败，因为未找到游戏主窗口");
                    //Console.WriteLine("未找到主窗口句柄");
                    return;
                }

                // 确保窗口没有最小化
                ShowWindow(hWnd, SW_RESTORE);

                // 将窗口置于前台
                SetForegroundWindow(hWnd);

                // 延迟确保窗口时间响应
                await Task.Delay(10);

                // 模拟Ctrl+V粘贴操作
                keybd_event((byte)KeyboardKeyStates.VK_CONTROL, 0, 0, UIntPtr.Zero); // 按下Ctrl键
                keybd_event((byte)KeyboardKeyStates.VK_V, 0, 0, UIntPtr.Zero); // 按下V键
                LauncherCore.WriteLog($"启动器核心：造图小工具 | 快捷指令 - 正在粘贴 {command} 指令到游戏中");
                keybd_event((byte)KeyboardKeyStates.VK_V, 0, (uint)KeyboardKeyStates.KEYEVENTF_KEYUP, UIntPtr.Zero); // 释放V键
                keybd_event((byte)KeyboardKeyStates.VK_CONTROL, 0, (uint)KeyboardKeyStates.KEYEVENTF_KEYUP, UIntPtr.Zero); // 释放Ctrl键
                keybd_event(0x0D, 0, 0, UIntPtr.Zero); // 按下回车键
                LauncherCore.WriteLog($"启动器核心：造图小工具 | 快捷指令 - 正在执行 {command} 指令到游戏中");
                keybd_event(0x0D, 0, (uint)KeyboardKeyStates.KEYEVENTF_KEYUP, UIntPtr.Zero); // 释放回车键
                LauncherCore.WriteLog($"启动器核心：造图小工具 | 快捷指令 - 执行 {command} 指令到游戏中成功");
                NoticeManager.NotificationShow.AddNotification(new NotificationModel()
                {
                    Title = "造图小助手",
                    Content = $"类型：快捷指令\n事件：写入链接到游戏聊天栏中执行",
                    NotificationType = AduSkin.Controls.EnumPromptType.Success
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine("发生错误: " + ex.Message);
            }
        }

        private async void ToMinecraftCommand(string command)
        {
            Clipboard.SetDataObject(command);
            LauncherCore.WriteLog($"启动器核心：造图小工具 | 快捷指令 - 已复制指令{command}，准备粘贴到游戏中");
            await SendMinecraftCommandToProcessAsync(GameStarting.Pid, command);
            //Console.WriteLine($"{command} 已执行");
        }

        private async Task SendPasteCommandToProcess(int processId, int id = 0)
        {
            try
            {
                Process proc = Process.GetProcessById(processId);
                if (proc == null)
                {
                    //Console.WriteLine("未找到进程");
                    LauncherCore.WriteLog($"启动器核心：造图小工具 | 表情包大厅 - 粘贴ID为{id}图片的链接失败，因为未找到游戏进程");
                    return;
                }

                IntPtr hWnd = proc.MainWindowHandle;
                if (hWnd == IntPtr.Zero)
                {
                    LauncherCore.WriteLog($"启动器核心：造图小工具 | 表情包大厅 - 粘贴ID为{id}图片的链接失败，因为未找到游戏主窗口");
                    //Console.WriteLine("未找到主窗口句柄");
                    return;
                }

                // 确保窗口没有最小化
                ShowWindow(hWnd, SW_RESTORE);

                // 将窗口置于前台
                SetForegroundWindow(hWnd);

                // 延迟确保窗口时间响应
                await Task.Delay(10);

                // 模拟Ctrl+V粘贴操作
                keybd_event((byte)KeyboardKeyStates.VK_CONTROL, 0, 0, UIntPtr.Zero); // 按下Ctrl键
                keybd_event((byte)KeyboardKeyStates.VK_V, 0, 0, UIntPtr.Zero); // 按下V键
                keybd_event((byte)KeyboardKeyStates.VK_V, 0, (uint)KeyboardKeyStates.KEYEVENTF_KEYUP, UIntPtr.Zero); // 释放V键
                keybd_event((byte)KeyboardKeyStates.VK_CONTROL, 0, (uint)KeyboardKeyStates.KEYEVENTF_KEYUP, UIntPtr.Zero); // 释放Ctrl键
                                                                                                                           //Console.WriteLine("操作完成");
                LauncherCore.WriteLog($"启动器核心：造图小工具 | 表情包大厅 - 粘贴ID为{id}图片的链接成功");
                NoticeManager.NotificationShow.AddNotification(new NotificationModel()
                {
                    Title = "造图小助手",
                    Content = $"类型：表情包\nID：{id}\n事件：写入链接到游戏输入框中",
                    NotificationType = AduSkin.Controls.EnumPromptType.Success
                });
            }
            catch (Exception ex)
            {
                LauncherCore.WriteLog($"启动器核心：造图小工具 | 表情包大厅 - 粘贴ID为{id}图片的链接失败，{ex.Message}");
                Console.WriteLine("发生错误: " + ex.Message);
            }
        }

        private async void ContentPaste_Click(object sender, RoutedEventArgs e)
        {
            if (sender is MenuItem button && button.DataContext is ImageItem item)
            {
                Clipboard.SetDataObject(item.Url);
                LauncherCore.WriteLog($"启动器核心：造图小工具 | 表情包大厅 - 右键菜单信息，复制ID为{item.Id}的图片链接，正在准备粘贴到游戏中");
                await SendPasteCommandToProcess(GameStarting.Pid, item.Id);
                //Console.WriteLine($"{item.Url} 已复制");
            }
        }

        private void ContentCopy_Click(object sender, RoutedEventArgs e)
        {
            if (sender is MenuItem button && button.DataContext is ImageItem item)
            {
                Clipboard.SetDataObject(item.Url);
                LauncherCore.WriteLog($"启动器核心：造图小工具 | 表情包大厅 - 右键菜单信息，复制ID为{item.Id}的图片链接成功");
                NoticeManager.NotificationShow.AddNotification(new NotificationModel()
                {
                    Title = "造图小助手",
                    Content = $"类型：表情包\nID：{item.Id}\n事件：复制链接",
                    NotificationType = AduSkin.Controls.EnumPromptType.Success
                });
            }
        }

        public async void UpdateMainLoadingStats(UpdateMainLoadingStatsType type)
        {
            switch (type)
            {
                case UpdateMainLoadingStatsType.Visible:
                    MainIndex.Visibility = Visibility.Hidden;
                    MainLoading.Visibility = Visibility.Visible;
                    break;
                case UpdateMainLoadingStatsType.Loading:
                    OpenInAppIcon.Visibility = Visibility.Hidden;
                    FolderOpenIcon.Visibility = Visibility.Hidden;
                    VectorLinkIcon.Visibility = Visibility.Hidden;
                    NLoadingIcon.Visibility = Visibility.Visible;
                    LauncherCore.WriteLog($"启动器核心：造图小工具 - 等待游戏加载中...");
                    break;
                case UpdateMainLoadingStatsType.OpenInApp:
                    NLoadingIcon.Visibility = Visibility.Hidden;
                    VectorLinkIcon.Visibility = Visibility.Hidden;
                    FolderOpenIcon.Visibility = Visibility.Hidden;
                    OpenInAppIcon.Visibility = Visibility.Visible;
                    LauncherCore.WriteLog($"启动器核心：造图小工具 - 等待游戏操作中...");
                    break;
                case UpdateMainLoadingStatsType.FolderOpen:
                    NLoadingIcon.Visibility = Visibility.Hidden;
                    OpenInAppIcon.Visibility = Visibility.Hidden;
                    VectorLinkIcon.Visibility = Visibility.Hidden;
                    FolderOpenIcon.Visibility = Visibility.Visible;
                    LauncherCore.WriteLog($"启动器核心：造图小工具 - 游戏已进入地图存档");
                    break;
                case UpdateMainLoadingStatsType.Hidden:
                    NLoadingIcon.Visibility = Visibility.Hidden;
                    await Task.Delay(1000);
                    MainLoading.Visibility = Visibility.Hidden;
                    MainIndex.Visibility = Visibility.Visible;
                    break;
                case UpdateMainLoadingStatsType.TipsVisible:
                    MainLoadingTips.Visibility = Visibility.Visible;
                    break;
                case UpdateMainLoadingStatsType.TipsHidden:
                    MainLoadingTips.Visibility = Visibility.Hidden;
                    break;
                case UpdateMainLoadingStatsType.VectorLink:
                    NLoadingIcon.Visibility = Visibility.Hidden;
                    OpenInAppIcon.Visibility = Visibility.Hidden;
                    FolderOpenIcon.Visibility = Visibility.Hidden;
                    VectorLinkIcon.Visibility = Visibility.Visible;
                    LauncherCore.WriteLog($"启动器核心：造图小工具 - 游戏已进入多人服务器");
                    break;
            }
        }

        public void UpdateMainLoadingTips(string text)
        {
            MainLoadingTips.Text = text;
        }

        public void SaveInfoVisibleCollapsed()
        {
            SaveInfo.Visibility = Visibility.Collapsed;
        }

        public void SaveInfoVisibleVisible()
        {
            SaveInfo.Visibility = Visibility.Visible;
        }

        public void UpdateSaveInfo(string SaveTitle, string SaveName, string SavePath)
        {
            UseSaveTitle.Text = SaveTitle;
            UseSaveName.Text = SaveName;
            UseSavePath.Text = SavePath;
            LauncherCore.WriteLog($"启动器核心：造图小工具 - 已获取到当前单人游戏的地图存档信息，地图标题：{SaveTitle} 地图名称：{SaveName}，地图位置：{SavePath}");
        }

        public void LocalAreaNetworkGameInfoCollapsed()
        {
            LocalAreaNetworkGameInfo.Visibility = Visibility.Collapsed;
        }

        public void LocalAreaNetworkGameInfoVisible()
        {
            LocalAreaNetworkGameInfo.Visibility = Visibility.Visible;
        }

        public void UpdateLocalAreaNetworkGameInfo(string MaxPlayerCount, string CurrentPlayerCount, string Port)
        {
            LocalAreaNetworkGameInfo_MaxPlayerCount.Text = MaxPlayerCount;
            LocalAreaNetworkGameInfo_CurrentPlayerCount.Text = CurrentPlayerCount;
            LocalAreaNetworkGameInfo_Port.Text = Port;
            LauncherCore.WriteLog($"启动器核心：造图小工具 - 检测到游戏开启局域网游戏，端口：{Port}");
        }

        private void LocalAreaNetworkGameInfo_CopyPort_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(LocalAreaNetworkGameInfo_Port.Text))
            {
                Clipboard.SetText(LocalAreaNetworkGameInfo_Port.Text);
                NoticeManager.NotificationShow.AddNotification(new NotificationModel()
                {
                    Title = "造图小助手",
                    Content = $"类型：局域网游戏\n端口号：{LocalAreaNetworkGameInfo_Port.Text}\n事件：复制端口号",
                    NotificationType = AduSkin.Controls.EnumPromptType.Success
                });
            }
        }

        private void OpenUseSavePath_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(UseSavePath.Text) && Directory.Exists(UseSavePath.Text))
            {
                try
                {
                    Process.Start("explorer.exe", UseSavePath.Text);
                    LauncherCore.WriteLog($"启动器核心：造图小工具 | 地图信息 - 打开存档 {UseSavePath.Text} 成功");
                }
                catch (Exception)
                {
                    LauncherCore.WriteLog($"启动器核心：造图小工具 | 地图信息 - 打开存档 {UseSavePath.Text} 失败");
                }

            }
        }

        private void OpenNBTExplorer_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(UseSavePath.Text) && Directory.Exists(UseSavePath.Text))
            {
                LauncherTools.NBTExplorer.Use(Path.Combine(UseSavePath.Text, "level.dat"));
                LauncherCore.WriteLog($"启动器核心：造图小工具 | 地图信息 - 使用 NBTExplorer 打开存档 {UseSavePath.Text} 成功");
            }
        }

        public void MultiplayerGameInfoCollapsed()
        {
            MultiplayerGameInfo.Visibility = Visibility.Collapsed;
        }

        public void MultiplayerGameInfoVisible()
        {
            MultiplayerGameInfo.Visibility = Visibility.Visible;
        }

        public void UpdateMultiplayerGameInfo(string ServerMotd, string MaxPlayerCount, string CurrentPlayerCount, string IpAndPort, string AveragePingTime)
        {
            MultiplayerGameInfo_ServerMotd.Text = ServerMotd;
            MultiplayerGameInfo_MaxPlayerCount.Text = MaxPlayerCount;
            MultiplayerGameInfo_CurrentPlayerCount.Text = CurrentPlayerCount;
            MultiplayerGameInfo_IpAndPort.Text = IpAndPort;
            MultiplayerGameInfo_AveragePingTime.Text = AveragePingTime;
            LauncherCore.WriteLog($"启动器核心：造图小工具 - 已获取到当前多人游戏的服务器信息，标题：{ServerMotd}，IP和端口{IpAndPort}，人数：{CurrentPlayerCount}/{MaxPlayerCount}，延迟：{AveragePingTime}");
        }

        private void MultiplayerGameInfo_CopyIpAndPort_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(MultiplayerGameInfo_IpAndPort.Text))
            {
                Clipboard.SetText(MultiplayerGameInfo_IpAndPort.Text);
                LauncherCore.WriteLog($"启动器核心：造图小工具 | 地图信息 - 复制多人游戏服务器IP与端口号成功");
                NoticeManager.NotificationShow.AddNotification(new NotificationModel()
                {
                    Title = "造图小助手",
                    Content = $"类型：多人游戏\nIP与端口号：{MultiplayerGameInfo_IpAndPort.Text}\n事件：复制IP与端口号",
                    NotificationType = AduSkin.Controls.EnumPromptType.Success
                });
            }
        }

        private void ShortCutCommand_Time_Morning_Click(object sender, RoutedEventArgs e)
        {
            ToMinecraftCommand("/time set day");
        }

        private void ShortCutCommand_Time_Noon_Click(object sender, RoutedEventArgs e)
        {
            ToMinecraftCommand("/time set 6000");
        }

        private void ShortCutCommand_Time_Evening_Click(object sender, RoutedEventArgs e)
        {
            ToMinecraftCommand("/time set night");
        }

        private void ShortCutCommand_Time_Midnight_Click(object sender, RoutedEventArgs e)
        {
            ToMinecraftCommand("/time set 18000");
        }

        private void ShortCutCommand_Weather_HeavyRainDay_Click(object sender, RoutedEventArgs e)
        {
            ToMinecraftCommand("/weather thunder");
        }

        private void ShortCutCommand_Weather_RainyDay_Click(object sender, RoutedEventArgs e)
        {
            ToMinecraftCommand("/weather rain");
        }

        private void ShortCutCommand_Weather_SunnyDay_Click(object sender, RoutedEventArgs e)
        {
            ToMinecraftCommand("/weather clear");
        }

        private void ShortCutCommand_Gamerules_commandBlockOutput_true_Click(object sender, RoutedEventArgs e)
        {
            ToMinecraftCommand("/gamerule commandBlockOutput true");
        }

        private void ShortCutCommand_Gamerules_commandBlockOutput_false_Click(object sender, RoutedEventArgs e)
        {
            ToMinecraftCommand("/gamerule commandBlockOutput false");
        }

        private void ShortCutCommand_Gamerules_keepInventory_false_Click(object sender, RoutedEventArgs e)
        {
            ToMinecraftCommand("/gamerule keepInventory false");
        }

        private void ShortCutCommand_Gamerules_keepInventory_true_Click(object sender, RoutedEventArgs e)
        {
            ToMinecraftCommand("/gamerule keepInventory true");
        }
    }
}

public enum UpdateMainLoadingStatsType
{
    Visible = 0,
    Loading = 1,
    OpenInApp = 2,
    FolderOpen = 3,
    VectorLink = 4,
    TipsVisible = 5,
    TipsHidden = 6,
    Hidden = 7
}

public class ImageItem
{
    public int Id { get; set; }
    public string Url { get; set; }
    public int Type { get; set; }
    public string Tag { get; set; }
    public BitmapImage Image { get; set; } // 用于存储加载的图片
}
