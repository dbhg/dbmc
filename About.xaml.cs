﻿using DBMC_Core;
using System;
using System.Reflection;
using System.Windows;
using System.Windows.Input;

namespace DBMC
{
    /// <summary>
    /// About.xaml 的交互逻辑
    /// </summary>
    public partial class About : Window
    {
        private bool isDragging;
        private Point startPoint;

        public About()
        {
            InitializeComponent();
            Version version = Assembly.GetExecutingAssembly().GetName().Version;
            string versionString = $"客户端版本：{version.Major}.{version.Minor}.{version.Build}.{version.Revision}";
            VersionText.Text = versionString;
            Loaded +=  delegate
            {
                LauncherCore.WriteLog($"启动器核心：关于 - 窗口打开正常");
               LauncherCore.WriteLog($"启动器核心：关于 - 成功获取软件版本号：{VersionText.Text}");
            };
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isDragging = true;
            startPoint = e.GetPosition(this);
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Point currentPoint = e.GetPosition(this);
                this.Left += currentPoint.X - startPoint.X;
                this.Top += currentPoint.Y - startPoint.Y;
            }
        }

        private void Window_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            isDragging = false;
        }

        private void ExitApp_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Minimize_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void GetUpdate_Click(object sender, RoutedEventArgs e)
        {
            LauncherCore.WriteLog($"启动器核心：关于 - 用户点击了“检查更新”");
            Update update = new Update();
            update.ShowDialog();
        }
    }
}
